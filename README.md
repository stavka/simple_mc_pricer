# Simple MC Pricer

## Assumptions: 

1. simple BS model calibrated to historical prices (using historical daily vol of last 1000 days)
2. All BS assumptions apply
3. Per instructions code is using cvs file with data and not API
4. Using simplest volatility estimator (realized)
5. Daily simulation timesteps (fixed for simplicty)
6. All classes in one module for simplicity too

## Usage

1. put all files in the same directory
2. See unit tests and main() for example

