# -*- coding: utf-8 -*-
"""
@author: victor.istratov
"""
import datetime
import numpy as np
import pandas as pd
from math import sqrt
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
import logging

logging.basicConfig(level=logging.DEBUG)

class BSModel():
    
    def __init__(self, rfr, dividends):  
        self.rfr = rfr
        self.divdends = dividends
        self.dt = 1.0/252.0
        self.antithetic = False
    
    def evolution(self, S, t):
        ''' S is array '''

        #model ds/s = mu dt + sigma dW
        std = self.sigma*sqrt(self.dt)
        mean = self.rfr*self.dt-std*std/2.0
        
        if self.antithetic:
            r = np.random.normal(mean, std, len(S)//2)
            r = np.concatenate((r,2.*mean-r))
        else:
            r = np.random.normal(mean, std, len(S))
   
        #correct distribution of sample
        r = np.subtract( r, np.mean(r)-mean )
        r = np.multiply( r, std/np.std(r) ) 
        
        
        
        div = self.divdends.get(t,0)
        if(div):
            logging.debug('Div date found %s %s'%(str(t),str(div))) 
          
        return np.multiply(S, np.exp(r))-div
    
    def calibrate(self, data):
        
        self.sigma = np.std(data)*sqrt(252.)
        logging.debug('BS Model calibrated to historical vol %s'%str(self.sigma)) 
   

class MCPricer():
    '''
    '''
    
    def __init__(self, numpaths, model, S0):
        self.numpaths = numpaths
        self.model= model
        self.S0 = S0

    def generate(self, T):
        
        us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())
        self.calendar =  pd.DatetimeIndex( start=datetime.date.today(),
                                      end=T,
                                      freq=us_bd)
        
        Snew = np.full( self.numpaths, self.S0 )
        
        np.random.seed(12345)
        
        for day in self.calendar:
            Sold = Snew
            Snew = self.model.evolution(Sold, day.date())
        
        self.ST = Snew
          
        
    def price(self, inst):
        
        self.generate(inst.maturity)
        
        inst_values = inst.payoff(self.ST)
        
        years = len(self.calendar)/252.
        
        return np.mean(inst_values)*np.exp(-self.model.rfr*years)
        
    
    
class EuropeanOption():
    def __init__(self, maturity, strike ):
        self.maturity = maturity
        self.strike = strike
    
class EuropeanCall(EuropeanOption):
    
    def payoff(self, S):
        
        return np.maximum(np.subtract( S, self.strike), 0)
   
class EuropeanPut(EuropeanOption):
    
    def payoff(self, S):
        
        return np.maximum(np.subtract( self.strike, S ), 0)       
        
        
def main():
    
    
    data = pd.read_csv('EOD-HD.csv', index_col=0, parse_dates=True)
    data.sort_index( ascending=True, inplace=True)
    data = data.iloc[-1000:]  # take last 1k entries
    data['return'] = np.log(data['Adj_Close']).diff()   
    data.dropna(inplace = True)
        
    maturity = datetime.date.today() + datetime.timedelta(days=365)
    
    us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())
    calendar =  pd.DatetimeIndex( start=datetime.date.today(),
                                      end=maturity,
                                      freq=us_bd)
    
    div_schedule = { calendar[28].date() : 1.03,
                     calendar[110].date() : 1.05,
                     calendar[200].date() : 0.95, }
    #div_schedule = {}
    
    
    model = BSModel(  0.05, # risk free rate
                      div_schedule, # dividends
                     )
    model.calibrate( data['return'].as_matrix() )
    pricer = MCPricer(1000000, model, 184.0)    
    
    inst = EuropeanCall(maturity, 180.)
    
    # convergence test
    for i in (1000,10000,100000,1000000):
            
        pricer = MCPricer(i, model, 184.0)

        print( (i, pricer.price(inst)) )    
        
        
if __name__ == "__main__":
    main()       