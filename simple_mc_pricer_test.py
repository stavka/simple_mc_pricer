import unittest
import simple_mc_pricer as smp
import datetime
import pandas as pd
import numpy as np
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
 


class TestOptionBSMCPricer(unittest.TestCase):
 
    def setUp(self):
        
        #reading data
        self.data = pd.read_csv('EOD-HD.csv', index_col=0, parse_dates=True)
        self.data.sort_index( ascending=True, inplace=True)
        self.data = self.data.iloc[-1000:]  # take last 1k entries
        self.data['return'] = np.log(self.data['Adj_Close']).diff()   
        self.data.dropna(inplace = True)
        
        #test setup
        self.maturity = datetime.date.today() + datetime.timedelta(days=365)
        current_price = 184.
        risk_free_rate = 0.05
        
        us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())
        calendar =  pd.DatetimeIndex( start=datetime.date.today(),
                                      end=self.maturity,
                                      freq=us_bd)
    
        div_schedule = { calendar[28].date() : 1.03,
                     calendar[110].date() : 1.05,
                     calendar[200].date() : 0.95, }
        
        self.model = smp.BSModel( risk_free_rate,
                                  div_schedule,
                                 )
        
        self.model.calibrate( self.data['return'].as_matrix() )
        self.pricer = smp.MCPricer(1000000, self.model, current_price)
        
        
 
    def test_Call(self):
        
        c = smp.EuropeanCall(self.maturity, 180.)
        
        price = self.pricer.price(c)
               
        print(price)
        
        self.assertAlmostEqual( price, 18.5, places=1 )
 

 
if __name__ == '__main__':
    unittest.main()